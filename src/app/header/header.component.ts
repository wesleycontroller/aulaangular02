import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  @Input() dataTable: any

  @Output() emissao: EventEmitter<any> = new EventEmitter

  constructor() { 
  }

  ngOnInit() {
    console.log(this.dataTable)
  }

  setToDad(item:any) {
    this.emissao.emit(item)
  }

}
