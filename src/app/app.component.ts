import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent{

  data2: any[] = [
    {id: 1, name: "Wesley"},
    {id: 2, name: "Glailson"},
    {id: 3, name: "Wladmir"},
    {id: 4, name: "Pedro"},
  ]

  recebido: any 

  constructor() {}

  ngOnInit(): void {
    
  }

  recptInDad(event:any) {
    this.recebido = event
  }

}
